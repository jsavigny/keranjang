# Keranjang
Mini e-Commerce Cart System Built in Golang and GraphQL.

The main logic of the challenge lies in the `order/service/order_service.go` file.

# Prerequisites & Dependencies
- MySQL >= 5.7
- Go >= 1.14

# Getting Started
1. Clone this repo
2. Install prerequisites
3. Install dependencies `make dep`
4. Edit `config.json` to match your environment
5. Init the database `mysql -u $YOUR_USER -p$YOUR_PWD < ./keranjang.sql`
5. Build and run `make build && ./bin/keranjang` OR run `make run`

NB: To see the test, run `make test`

# GraphQL Schema
The schema is defined in `order/delivery/graphql/schema.graphql`
```
type Mutation {
  AddToCart(id: Integer, items: [ItemInput]): Order
  Checkout(cart_id: Integer): Order
}

type Item {
  sku: String
  name: String
  price: Float
  quantity: Int
}

type ItemInput {
  sku: String
  quantity: Int
}

type Order {
  id: Int
  total: Float
  sub_total: Float
  reduction: Float
  items: [Item]
  state: String
}

```

Currently support two (2) mutations, `AddToCart` and `Checkout`

## Mutations

### AddToCart
Request:
`curl -X POST -H "Content-Type: application/json" -d '{"query": "mutation { AddToCart(id:1,items: [{sku: \"234234\", quantity: 1}, {sku: \"43N23P\", quantity: 1}]) {id total sub_total, reduction, state, items{ sku name price quantity}}  }" }' localhost:9292/graphql`

Response:
```
{
        "data": {
                "AddToCart": {
                        "id": 1,
                        "items": [
                                {
                                        "name": "Raspberry Pi B",
                                        "price": 30,
                                        "quantity": 1,
                                        "sku": "234234"
                                },
                                {
                                        "name": "MacBook Pro",
                                        "price": 5399.99,
                                        "quantity": 1,
                                        "sku": "43N23P"
                                }
                        ],
                        "reduction": 0,
                        "state": "Cart",
                        "sub_total": 5429.99,
                        "total": 5429.99
                }
        }
}
```

### Checkout
Request:
`curl -X POST -H "Content-Type: application/json" -d '{"query": "mutation { Checkout(id: 1) {id total sub_total, reduction, state, items{ sku name price quantity}}  }" }' localhost:9292/graphql`

Response:
```
{
        "data": {
                "Checkout": {
                        "id": 1,
                        "items": [
                                {
                                        "name": "Raspberry Pi B",
                                        "price": 30,
                                        "quantity": 1,
                                        "sku": "234234"
                                },
                                {
                                        "name": "MacBook Pro",
                                        "price": 5399.99,
                                        "quantity": 1,
                                        "sku": "43N23P"
                                }
                        ],
                        "reduction": 30,
                        "state": "CheckedOut",
                        "sub_total": 5429.99,
                        "total": 5399.99
                }
        }
}
```
