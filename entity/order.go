package entity

type OrderState int64

func (os OrderState) String() string {
	return [...]string{"Cart", "CheckedOut"}[os]
}

const (
	Cart OrderState = iota
	Checkout
)

type Order struct {
	ID        int64      `json:"id"`
	State     OrderState `json:"state"`
	SubTotal  float64    `json:"sub_total"`
	Reduction float64    `json:"reduction"`
	Total     float64    `json:"total"`
}
