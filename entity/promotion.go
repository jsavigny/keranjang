package entity

type PromotionType int64

func (pt PromotionType) String() string {
	return [...]string{"FreeProduct", "Discount"}[pt]
}

const (
	FreeProduct PromotionType = iota
	Discount
)

type Promotion struct {
	ID                 int64         `json:"id"`
	Type               PromotionType `json:"state"`
	ProductSKU         string        `json:"product_sku"`
	FreeProductSKU     string        `json:"free_product_sku"`
	MinQuantity        int64         `json:"min_quantity"`
	DiscountPercentage float64       `json:"discount_percentage"`
}
