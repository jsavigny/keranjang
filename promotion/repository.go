package promotion

import "keranjang/entity"

type Repository interface {
	GetAll() ([]*entity.Promotion, error)
}
