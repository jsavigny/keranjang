// Code generated by mockery v2.0.4. DO NOT EDIT.

package mocks

import (
	entity "keranjang/entity"

	mock "github.com/stretchr/testify/mock"
)

// Repository is an autogenerated mock type for the Repository type
type Repository struct {
	mock.Mock
}

// GetAll provides a mock function with given fields:
func (_m *Repository) GetAll() ([]*entity.Promotion, error) {
	ret := _m.Called()

	var r0 []*entity.Promotion
	if rf, ok := ret.Get(0).(func() []*entity.Promotion); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*entity.Promotion)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
