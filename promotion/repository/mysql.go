package repository

import (
	"database/sql"
	"keranjang/entity"
	"keranjang/promotion"
)

type mysqlRepository struct {
	Conn *sql.DB
}

func NewMysqlRepository(Conn *sql.DB) promotion.Repository {
	return &mysqlRepository{Conn}
}

func (m *mysqlRepository) fetch(query string, args ...interface{}) ([]*entity.Promotion, error) {
	rows, err := m.Conn.Query(query, args...)
	if err != nil {
		return nil, err
	}

	defer func() {
		rows.Close()
	}()

	result := make([]*entity.Promotion, 0)
	for rows.Next() {
		t := new(entity.Promotion)
		err = rows.Scan(
			&t.ID,
			&t.Type,
			&t.ProductSKU,
			&t.FreeProductSKU,
			&t.MinQuantity,
			&t.DiscountPercentage,
		)

		if err != nil {
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlRepository) GetAll() (res []*entity.Promotion, err error) {
	query := `SELECT id,type,product_sku, free_product_sku, min_quantity, discount_percentage
  						FROM promotions WHERE 1=1`

	list, err := m.fetch(query)
	if err != nil {
		return nil, err
	}

	return list, nil
}
