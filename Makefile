test:
	go test -v -cover -covermode=atomic ./...

dep:
	go mod download

build:
	go build -o ./bin/keranjang main.go

run:
	go run main.go

.PHONY: test dep build run
