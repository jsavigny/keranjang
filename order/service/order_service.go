package service

import (
	"fmt"
	"keranjang/entity"
	"keranjang/line_item"
	"keranjang/order"
	"keranjang/product"
	"keranjang/promotion"
	"math"
)

type orderService struct {
	productRepo  product.Repository
	orderRepo    order.Repository
	lineItemRepo line_item.Repository
	promoRepo    promotion.Repository
}

func NewOrderService(pr product.Repository, or order.Repository, lr line_item.Repository, pr_r promotion.Repository) order.Service {
	return &orderService{
		productRepo:  pr,
		orderRepo:    or,
		lineItemRepo: lr,
		promoRepo:    pr_r,
	}
}

func (os *orderService) AddToCart(id int64, items []*entity.LineItem) (*entity.Order, error) {
	var (
		order *entity.Order
		err   error
	)

	order, err = os.orderRepo.GetByID(id)
	if err != nil {
		return order, err
	}

	if order.State.String() != "Cart" {
		return order, fmt.Errorf("Invalid State Transition")
	}

	addedTotal := 0.0
	for _, item := range items {
		sku := item.SKU
		quantity := item.Quantity

		product, err := os.productRepo.GetBySKU(sku)
		if err != nil {
			return order, err
		}

		if product.Stock < quantity {
			return order, fmt.Errorf("Stock is not enough")
		}

		product.Stock -= quantity

		err = os.productRepo.Update(product)
		if err != nil {
			return order, err
		}

		lineItem := entity.LineItem{OrderID: id, SKU: product.SKU, Price: product.Price, Quantity: quantity, Name: product.Name}
		err = os.lineItemRepo.Upsert(&lineItem)
		if err != nil {
			return order, err
		}

		addedTotal += lineItem.Price * float64(lineItem.Quantity)
	}

	price := math.Round(addedTotal*100) / 100
	order.SubTotal += price
	order.Total += price

	fmt.Print(order.ID)
	err = os.orderRepo.Update(order)
	if err != nil {
		return order, err
	}

	return order, nil
}

func (os *orderService) Checkout(id int64) (*entity.Order, error) {
	var (
		order *entity.Order
		err   error
	)

	order, err = os.orderRepo.GetByID(id)
	if err != nil {
		return order, err
	}

	lineItems, err := os.lineItemRepo.GetByOrderID(id)
	if err != nil {
		return order, err
	}

	if len(lineItems) == 0 {
		return order, fmt.Errorf("No Items on Cart")
	}

	if order.State.String() != "Cart" {
		return order, fmt.Errorf("Invalid State Transition")
	}

	promos, err := os.promoRepo.GetAll()
	if err != nil {
		return order, err
	}

	reduction := 0.0
	for _, lineItem := range lineItems {
		for _, promo := range promos {
			if promo.Type.String() == "FreeProduct" {
				if lineItem.SKU == promo.ProductSKU && lineItem.Quantity >= promo.MinQuantity {
					for _, free_item := range lineItems {
						if free_item.SKU == promo.FreeProductSKU {
							reduction += free_item.Price
						}
					}
				}
			} else if promo.Type.String() == "Discount" {
				if lineItem.SKU == promo.ProductSKU && lineItem.Quantity >= promo.MinQuantity {
					reduction += lineItem.Price * float64(lineItem.Quantity) * promo.DiscountPercentage
				}
			}
		}
	}

	order.Reduction = reduction
	order.Total = math.Round((order.SubTotal-order.Reduction)*100) / 100
	order.State = 1 // Checkout
	err = os.orderRepo.Update(order)
	if err != nil {
		return order, err
	}

	return order, nil
}
