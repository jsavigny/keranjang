package service_test

import (
	"fmt"
	"keranjang/entity"
	lineItemMocks "keranjang/line_item/mocks"
	orderMocks "keranjang/order/mocks"
	productMocks "keranjang/product/mocks"
	promoMocks "keranjang/promotion/mocks"

	"testing"

	"keranjang/order/service"

	"github.com/stretchr/testify/assert"
)

func TestAddToCart(t *testing.T) {
	mockOrderRepo := new(orderMocks.Repository)
	mockProductRepo := new(productMocks.Repository)
	mockLineItemRepo := new(lineItemMocks.Repository)
	mockPromoRepo := new(promoMocks.Repository)

	inputLineItems := []*entity.LineItem{
		{
			SKU:      "120P90",
			Quantity: 1,
		},
	}
	mockOrder := entity.Order{
		ID:        69,
		State:     0,
		SubTotal:  0,
		Total:     0,
		Reduction: 0,
	}

	updatedMockOrder := entity.Order{
		ID:        69,
		State:     0,
		SubTotal:  49.99,
		Total:     49.99,
		Reduction: 0,
	}

	mockProduct := entity.Product{
		ID:    1,
		SKU:   "120P90",
		Name:  "Google Home",
		Price: 49.99,
		Stock: 10,
	}

	mockLineItem := entity.LineItem{
		OrderID:  69,
		SKU:      "120P90",
		Name:     "Google Home",
		Price:    49.99,
		Quantity: 1,
	}

	t.Run("success", func(t *testing.T) {
		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockProductRepo.On("GetBySKU", mockProduct.SKU).Once().Return(&mockProduct, nil)
		mockProductRepo.On("Update", &mockProduct).Once().Return(nil)
		mockLineItemRepo.On("Upsert", &mockLineItem).Once().Return(nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.NoError(t, err)
		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Order State is Invalid", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        69,
			State:     1,
			SubTotal:  50,
			Total:     50,
			Reduction: 0,
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.EqualError(t, err, "Invalid State Transition")
		mockOrderRepo.AssertExpectations(t)
	})

	t.Run("Quantity is more than stock", func(t *testing.T) {
		inputLineItems2 := []*entity.LineItem{
			{
				SKU:      "120P90",
				Quantity: 11,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockProductRepo.On("GetBySKU", mockProduct.SKU).Once().Return(&mockProduct, nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems2)
		assert.EqualError(t, err, "Stock is not enough")
		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
	})

	t.Run("Order Not Found", func(t *testing.T) {
		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(nil, fmt.Errorf("Order Not Found"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.EqualError(t, err, "Order Not Found")
		mockOrderRepo.AssertExpectations(t)
	})

	t.Run("Product Not Found", func(t *testing.T) {
		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockProductRepo.On("GetBySKU", mockProduct.SKU).Once().Return(nil, fmt.Errorf("Product Not Found"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.EqualError(t, err, "Product Not Found")
		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
	})

	t.Run("Update Product Error", func(t *testing.T) {
		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockProductRepo.On("GetBySKU", mockProduct.SKU).Once().Return(&mockProduct, nil)
		mockProductRepo.On("Update", &mockProduct).Once().Return(fmt.Errorf("Update Product Error"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.EqualError(t, err, "Update Product Error")
		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
	})

	t.Run("Line Item Upsert Error", func(t *testing.T) {
		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockProductRepo.On("GetBySKU", mockProduct.SKU).Once().Return(&mockProduct, nil)
		mockProductRepo.On("Update", &mockProduct).Once().Return(nil)
		mockLineItemRepo.On("Upsert", &mockLineItem).Once().Return(fmt.Errorf("Upsert Error"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.EqualError(t, err, "Upsert Error")
		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Update Order Error", func(t *testing.T) {
		mockOrder2 := entity.Order{
			ID:        69,
			State:     0,
			SubTotal:  0,
			Total:     0,
			Reduction: 0,
		}

		mockOrderRepo.On("GetByID", mockOrder2.ID).Once().Return(&mockOrder2, nil)
		mockProductRepo.On("GetBySKU", mockProduct.SKU).Once().Return(&mockProduct, nil)
		mockProductRepo.On("Update", &mockProduct).Once().Return(nil)
		mockLineItemRepo.On("Upsert", &mockLineItem).Once().Return(nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(fmt.Errorf("Update Order Error"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.AddToCart(69, inputLineItems)
		assert.EqualError(t, err, "Update Order Error")
		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})
}

func TestCheckout(t *testing.T) {
	mockOrderRepo := new(orderMocks.Repository)
	mockProductRepo := new(productMocks.Repository)
	mockLineItemRepo := new(lineItemMocks.Repository)
	mockPromoRepo := new(promoMocks.Repository)
	mockPromos := []*entity.Promotion{
		{
			ID:                 1,
			Type:               0, // Free Product
			ProductSKU:         "43N23P",
			FreeProductSKU:     "234234",
			MinQuantity:        1,
			DiscountPercentage: 0,
		},
		{
			ID:                 2,
			Type:               0, // Free Product
			ProductSKU:         "120P90",
			FreeProductSKU:     "120P90",
			MinQuantity:        3,
			DiscountPercentage: 0,
		},
		{
			ID:                 3,
			Type:               1, // Discount
			ProductSKU:         "A304SD",
			FreeProductSKU:     "",
			MinQuantity:        3,
			DiscountPercentage: 0.10,
		},
	}
	t.Run("Success No Promo", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        123,
			State:     0,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}

		updatedMockOrder := entity.Order{
			ID:        123,
			State:     1,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}

		mockLineItems := []*entity.LineItem{
			{
				OrderID:  123,
				ID:       22,
				SKU:      "120P90",
				Name:     "Google Home",
				Price:    49.99,
				Quantity: 1,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems, nil)
		mockPromoRepo.On("GetAll").Once().Return(mockPromos, nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(123)
		assert.NoError(t, err)

		mockOrderRepo.AssertExpectations(t)
		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Success Promo 1", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        456,
			State:     0,
			SubTotal:  5429.99,
			Total:     5429.99,
			Reduction: 0,
		}

		updatedMockOrder := entity.Order{
			ID:        456,
			State:     1,
			SubTotal:  5429.99,
			Total:     5399.99,
			Reduction: 30.00,
		}

		mockLineItems1 := []*entity.LineItem{
			{
				OrderID:  456,
				ID:       20,
				SKU:      "43N23P",
				Name:     "MacBook Pro",
				Price:    5399.99,
				Quantity: 1,
			},
			{
				OrderID:  456,
				ID:       21,
				SKU:      "234234",
				Name:     "Raspberry Pi B",
				Price:    30.00,
				Quantity: 1,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems1, nil)
		mockPromoRepo.On("GetAll").Once().Return(mockPromos, nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(456)
		assert.NoError(t, err)

		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Success Promo 2", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        789,
			State:     0,
			SubTotal:  149.97,
			Total:     149.97,
			Reduction: 0,
		}

		updatedMockOrder := entity.Order{
			ID:        789,
			State:     1,
			SubTotal:  149.97,
			Total:     99.98,
			Reduction: 49.99,
		}

		mockLineItems1 := []*entity.LineItem{
			{
				OrderID:  789,
				ID:       24,
				SKU:      "120P90",
				Name:     "Google Home",
				Price:    49.99,
				Quantity: 3,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems1, nil)
		mockPromoRepo.On("GetAll").Once().Return(mockPromos, nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(789)
		assert.NoError(t, err)

		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Success Promo 3", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        101,
			State:     0,
			SubTotal:  328.5,
			Total:     328.5,
			Reduction: 0,
		}

		updatedMockOrder := entity.Order{
			ID:        101,
			State:     1,
			SubTotal:  328.5,
			Total:     295.65,
			Reduction: 32.85,
		}

		mockLineItems1 := []*entity.LineItem{
			{
				OrderID:  101,
				ID:       25,
				SKU:      "A304SD",
				Name:     "Alexa Speaker",
				Price:    109.50,
				Quantity: 3,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems1, nil)
		mockPromoRepo.On("GetAll").Once().Return(mockPromos, nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(101)
		assert.NoError(t, err)

		mockProductRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Order Repo Error", func(t *testing.T) {
		mockOrderRepo.On("GetByID", int64(23)).Once().Return(nil, fmt.Errorf("Order Not Found"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(23)
		assert.Error(t, err)

		mockOrderRepo.AssertExpectations(t)
	})

	t.Run("Line Items Repo Error", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        123,
			State:     0,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(nil, fmt.Errorf("Line Items not Found"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(123)
		assert.Error(t, err)

		mockOrderRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Line Items Empty", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        123,
			State:     0,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}

		mockLineItems := []*entity.LineItem{}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems, nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(123)
		assert.EqualError(t, err, "No Items on Cart")

		mockOrderRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Order State Invalid", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        123,
			State:     1,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}
		mockLineItems := []*entity.LineItem{
			{
				OrderID:  123,
				ID:       22,
				SKU:      "120P90",
				Name:     "Google Home",
				Price:    49.99,
				Quantity: 1,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems, nil)

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(123)
		assert.EqualError(t, err, "Invalid State Transition")

		mockOrderRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
	})

	t.Run("Promo Repo Error", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        123,
			State:     0,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}
		mockLineItems := []*entity.LineItem{
			{
				OrderID:  123,
				ID:       22,
				SKU:      "120P90",
				Name:     "Google Home",
				Price:    49.99,
				Quantity: 1,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems, nil)
		mockPromoRepo.On("GetAll").Once().Return(nil, fmt.Errorf("Promo Repo Error"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(123)
		assert.EqualError(t, err, "Promo Repo Error")

		mockOrderRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
		mockPromoRepo.AssertExpectations(t)
	})

	t.Run("Update Order Error", func(t *testing.T) {
		mockOrder := entity.Order{
			ID:        123,
			State:     0,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}

		updatedMockOrder := entity.Order{
			ID:        123,
			State:     1,
			SubTotal:  49.99,
			Total:     49.99,
			Reduction: 0,
		}

		mockLineItems := []*entity.LineItem{
			{
				OrderID:  123,
				ID:       22,
				SKU:      "120P90",
				Name:     "Google Home",
				Price:    49.99,
				Quantity: 1,
			},
		}

		mockOrderRepo.On("GetByID", mockOrder.ID).Once().Return(&mockOrder, nil)
		mockLineItemRepo.On("GetByOrderID", mockOrder.ID).Once().Return(mockLineItems, nil)
		mockPromoRepo.On("GetAll").Once().Return(mockPromos, nil)
		mockOrderRepo.On("Update", &updatedMockOrder).Once().Return(fmt.Errorf("Update Order Error"))

		s := service.NewOrderService(mockProductRepo, mockOrderRepo, mockLineItemRepo, mockPromoRepo)

		_, err := s.Checkout(123)
		assert.EqualError(t, err, "Update Order Error")

		mockOrderRepo.AssertExpectations(t)
		mockLineItemRepo.AssertExpectations(t)
		mockPromoRepo.AssertExpectations(t)
	})
}
