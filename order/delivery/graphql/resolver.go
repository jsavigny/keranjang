package graphql

import (
	"fmt"
	"keranjang/entity"
	"keranjang/line_item"
	"keranjang/order"

	"github.com/graphql-go/graphql"
)

type OrderSerializer struct {
	ID        int64            `json:"id"`
	State     string           `json:"state"`
	SubTotal  float64          `json:"sub_total"`
	Reduction float64          `json:"reduction"`
	Total     float64          `json:"total"`
	Items     []ItemSerializer `json:"items"`
}

type ItemSerializer struct {
	SKU      string  `json:"sku"`
	Name     string  `json:"name"`
	Price    float64 `json:"price"`
	Quantity int64   `json:"quantity"`
}

type resolver struct {
	orderService    order.Service
	lineItemService line_item.Service
}

type Resolver interface {
	AddToCart(params graphql.ResolveParams) (interface{}, error)
	Checkout(params graphql.ResolveParams) (interface{}, error)
}

func NewResolver(oService order.Service, lService line_item.Service) Resolver {
	return &resolver{
		orderService:    oService,
		lineItemService: lService,
	}
}

func (r *resolver) AddToCart(params graphql.ResolveParams) (interface{}, error) {
	var (
		id         int
		inputItems []interface{}
		ok         bool
	)

	id, ok = params.Args["id"].(int)
	if !ok || id == 0 {
		return nil, fmt.Errorf("id is not integer or zero")
	}

	inputItems, ok = params.Args["items"].([]interface{})
	if !ok {
		return nil, fmt.Errorf("items error")
	}

	inputLineItems, err := parseItems(inputItems)
	if err != nil {
		return nil, fmt.Errorf("items error")
	}

	order, err := r.orderService.AddToCart(int64(id), inputLineItems)
	if err != nil {
		return nil, err
	}

	lineItems, err := r.lineItemService.GetByOrderID(int64(id))
	if err != nil {
		return nil, err
	}

	items := make([]ItemSerializer, 0)

	for _, lineItem := range lineItems {
		item := ItemSerializer{
			SKU:      lineItem.SKU,
			Name:     lineItem.Name,
			Quantity: lineItem.Quantity,
			Price:    lineItem.Price,
		}

		items = append(items, item)
	}

	ordS := OrderSerializer{
		ID:        order.ID,
		State:     order.State.String(),
		SubTotal:  order.SubTotal,
		Total:     order.Total,
		Reduction: order.Reduction,
		Items:     items,
	}
	return ordS, nil
}

func parseItems(inputItems []interface{}) ([]*entity.LineItem, error) {
	inputLineItems := make([]*entity.LineItem, 0)

	for _, inputItem := range inputItems {
		sku, ok := inputItem.(map[string]interface{})["sku"].(string)
		if !ok {
			return inputLineItems, fmt.Errorf("SKU is not string")
		}

		quantity, ok := inputItem.(map[string]interface{})["quantity"].(int)
		if !ok {
			return inputLineItems, fmt.Errorf("Quantity is not int")
		}

		inputLineItem := entity.LineItem{
			SKU:      sku,
			Quantity: int64(quantity),
		}
		inputLineItems = append(inputLineItems, &inputLineItem)
	}

	return inputLineItems, nil
}

func (r *resolver) Checkout(params graphql.ResolveParams) (interface{}, error) {
	var (
		id int
		ok bool
	)

	id, ok = params.Args["id"].(int)
	if !ok || id == 0 {
		return nil, fmt.Errorf("id is not integer or zero")
	}

	order, err := r.orderService.Checkout(int64(id))
	if err != nil {
		return nil, err
	}

	lineItems, err := r.lineItemService.GetByOrderID(int64(id))
	if err != nil {
		return nil, err
	}

	items := make([]ItemSerializer, 0)

	for _, lineItem := range lineItems {
		item := ItemSerializer{
			SKU:      lineItem.SKU,
			Name:     lineItem.Name,
			Quantity: lineItem.Quantity,
			Price:    lineItem.Price,
		}

		items = append(items, item)
	}

	ord := OrderSerializer{
		ID:        order.ID,
		State:     order.State.String(),
		SubTotal:  order.SubTotal,
		Total:     order.Total,
		Reduction: order.Reduction,
		Items:     items,
	}

	return ord, nil
}
