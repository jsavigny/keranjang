package graphql

import "github.com/graphql-go/graphql"

type Schema struct {
	orderResolver Resolver
}

// NewSchema initializes Schema struct which takes resolver as the argument.
func NewSchema(orderResolver Resolver) Schema {
	return Schema{
		orderResolver: orderResolver,
	}
}

// OrderGraphQL holds order information with graphql object
var OrderGraphQL = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Order",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"state": &graphql.Field{
				Type: graphql.String,
			},
			"total": &graphql.Field{
				Type: graphql.Float,
			},
			"sub_total": &graphql.Field{
				Type: graphql.Float,
			},
			"reduction": &graphql.Field{
				Type: graphql.Float,
			},
			"items": &graphql.Field{
				Type: graphql.NewList(ItemGraphQL),
			},
		},
	},
)

// ItemGraphQL holds item information with graphql object
var ItemGraphQL = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Item",
		Fields: graphql.Fields{
			"sku": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"price": &graphql.Field{
				Type: graphql.Float,
			},
			"quantity": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)

// ItemInputGraphQL holds item input information with graphql object
var ItemInputGraphQL = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "ItemInput",
		Fields: graphql.InputObjectConfigFieldMap{
			"sku": &graphql.InputObjectFieldConfig{
				Type: graphql.String,
			},
			"quantity": &graphql.InputObjectFieldConfig{
				Type: graphql.Int,
			},
		},
	},
)

// Mutation initializes config schema mutation for graphql server.
func (s Schema) Query() *graphql.Object {
	objectConfig := graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"DummyQuery": &graphql.Field{
				Type:        graphql.String,
				Description: "DummyQuery",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
					"sku": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
					"quantity": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: s.orderResolver.AddToCart,
			},
		},
	}

	return graphql.NewObject(objectConfig)
}

// Mutation initializes config schema mutation for graphql server.
func (s Schema) Mutation() *graphql.Object {
	objectConfig := graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"AddToCart": &graphql.Field{
				Type:        OrderGraphQL,
				Description: "Add  products to a cart",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
					"items": &graphql.ArgumentConfig{
						Type: graphql.NewList(ItemInputGraphQL),
					},
				},
				Resolve: s.orderResolver.AddToCart,
			},
			"Checkout": &graphql.Field{
				Type:        OrderGraphQL,
				Description: "Checkout a cart",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: s.orderResolver.Checkout,
			},
		},
	}

	return graphql.NewObject(objectConfig)
}
