package order

import "keranjang/entity"

type Repository interface {
	GetByID(id int64) (*entity.Order, error)
	Update(order *entity.Order) error
}
