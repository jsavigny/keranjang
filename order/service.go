package order

import "keranjang/entity"

type Service interface {
	AddToCart(id int64, items []*entity.LineItem) (*entity.Order, error)
	Checkout(id int64) (*entity.Order, error)
}
