package repository

import (
	"database/sql"
	"fmt"
	"keranjang/entity"
	"keranjang/order"
)

type mysqlRepository struct {
	Conn *sql.DB
}

func NewMysqlRepository(Conn *sql.DB) order.Repository {
	return &mysqlRepository{Conn}
}

func (m *mysqlRepository) fetch(query string, args ...interface{}) ([]*entity.Order, error) {
	rows, err := m.Conn.Query(query, args...)
	if err != nil {
		return nil, err
	}

	defer func() {
		rows.Close()
	}()

	result := make([]*entity.Order, 0)
	for rows.Next() {
		t := new(entity.Order)
		err = rows.Scan(
			&t.ID,
			&t.State,
			&t.SubTotal,
			&t.Total,
			&t.Reduction,
		)

		if err != nil {
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlRepository) GetByID(id int64) (res *entity.Order, err error) {
	query := `SELECT id,state,sub_total, total, reduction
  						FROM orders WHERE ID = ?`

	list, err := m.fetch(query, id)
	if err != nil {
		return nil, err
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return nil, fmt.Errorf("Order Not Found")
	}

	return res, nil
}

func (m *mysqlRepository) Update(or *entity.Order) error {
	query := `UPDATE orders set state=?, sub_total=?, total=?, reduction=? WHERE ID = ?`

	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return nil
	}

	res, err := stmt.Exec(or.State, or.SubTotal, or.Total, or.Reduction, or.ID)
	if err != nil {
		return err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", affect)

		return err
	}

	return nil
}
