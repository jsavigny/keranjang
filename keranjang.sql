CREATE DATABASE IF NOT EXISTS `keranjang`;
USE `keranjang`;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `state` tinyint NOT NULL DEFAULT 0,
  `sub_total` decimal(15,2) NOT NULL DEFAULT 0,
  `total` decimal(15,2) NOT NULL DEFAULT 0,
  `reduction` decimal(15,2) NOT NULL DEFAULT 0,
  `updated_at` datetime DEFAULT (NOW()),
  `created_at` datetime DEFAULT (NOW()),
  PRIMARY KEY (`id`)
);

LOCK TABLES `orders` WRITE;
INSERT INTO `orders` VALUES (1,0,0,0,0,NOW(),NOW());
UNLOCK TABLES;

DROP TABLE IF EXISTS `line_items`;
CREATE TABLE `line_items` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT 0,
  `quantity` int NOT NULL DEFAULT 0,
  `order_id` bigint,
  `updated_at` datetime DEFAULT (NOW()),
  `created_at` datetime DEFAULT (NOW()),
  PRIMARY KEY (`id`)
  -- FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`)
);

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` bigint NOT NULL AUTO_INCREMENT ,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT 0,
  `stock` int NOT NULL DEFAULT 0,
  `updated_at` datetime DEFAULT (NOW()),
  `created_at` datetime DEFAULT (NOW()),
  PRIMARY KEY (`id`)
);

LOCK TABLES `products` WRITE;
INSERT INTO `products` VALUES (1,'120P90','Google Home','49.99',10,NOW(),NOW()),(2,'43N23P','MacBook Pro','5399.99',5,NOW(),NOW()),(3,'A304SD','Alexa Speaker','109.5',10,NOW(),NOW()),(4,'234234','Raspberry Pi B','30',2,NOW(),NOW());
UNLOCK TABLES;

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions` (
  `id` bigint NOT NULL AUTO_INCREMENT ,
  `product_sku` varchar(255) NOT NULL,
  `free_product_sku` varchar(255) NOT NULL DEFAULT '',
  `discount_percentage` double NOT NULL DEFAULT 0,
  `type` int,
  `min_quantity` int NOT NULL DEFAULT 0,
  `updated_at` datetime DEFAULT (NOW()),
  `created_at` datetime DEFAULT (NOW()),
  PRIMARY KEY (`id`)
);

LOCK TABLES `promotions` WRITE;
INSERT INTO `promotions` VALUES (1,'43N23P','234234',0,0,1,NOW(),NOW()),(2,'120P90','120P90',0,0,3,NOW(),NOW()),(3,'A304SD','',0.10,1,3,NOW(),NOW());
UNLOCK TABLES;
