package line_item

import "keranjang/entity"

type Repository interface {
	GetByOrderID(order_id int64) ([]*entity.LineItem, error)
	Upsert(order *entity.LineItem) error
}
