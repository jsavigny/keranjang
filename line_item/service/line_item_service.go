package service

import (
	"keranjang/entity"
	"keranjang/line_item"
)

type lineItemService struct {
	lineItemRepo line_item.Repository
}

func NewOrderService(lr line_item.Repository) line_item.Service {
	return &lineItemService{
		lineItemRepo: lr,
	}
}

func (os *lineItemService) GetByOrderID(id int64) ([]*entity.LineItem, error) {
	lineItems, err := os.lineItemRepo.GetByOrderID(id)
	if err != nil {
		return lineItems, err
	}

	return lineItems, nil
}
