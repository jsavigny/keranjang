package repository

import (
	"database/sql"
	"keranjang/entity"
	"keranjang/line_item"
)

type mysqlRepository struct {
	Conn *sql.DB
}

func NewMysqlRepository(Conn *sql.DB) line_item.Repository {
	return &mysqlRepository{Conn}
}

func (m *mysqlRepository) fetch(query string, args ...interface{}) ([]*entity.LineItem, error) {
	rows, err := m.Conn.Query(query, args...)
	if err != nil {
		return nil, err
	}

	defer func() {
		rows.Close()
	}()

	result := make([]*entity.LineItem, 0)
	for rows.Next() {
		t := new(entity.LineItem)
		err = rows.Scan(
			&t.ID,
			&t.SKU,
			&t.Name,
			&t.Price,
			&t.Quantity,
			&t.OrderID,
		)

		if err != nil {
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlRepository) GetByOrderID(id int64) (res []*entity.LineItem, err error) {
	query := `SELECT id,sku,name, price, quantity, order_id
  						FROM line_items WHERE order_id = ?`

	list, err := m.fetch(query, id)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (m *mysqlRepository) insert(e *entity.LineItem) error {
	query := `INSERT line_items SET sku=?, name=?, price=?, quantity=?, order_id=?`
	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return err
	}

	res, err := stmt.Exec(e.SKU, e.Name, e.Price, e.Quantity, e.OrderID)
	if err != nil {
		return err
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		return err
	}

	e.ID = lastID
	return nil
}

func (m *mysqlRepository) update(e *entity.LineItem) error {
	query := `UPDATE line_items SET quantity=? WHERE ID = ?`
	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(e.Quantity, e.ID)
	if err != nil {
		return err
	}

	return nil
}

func (m *mysqlRepository) Upsert(e *entity.LineItem) error {
	existingLineItems, err := m.fetch(`SELECT id,sku,name, price, quantity, order_id
	FROM line_items WHERE order_id = ? AND sku = ?`, e.OrderID, e.SKU)
	if err != nil {
		return err
	}

	if len(existingLineItems) == 0 { // Not exist
		return m.insert(e)
	} else if len(existingLineItems) == 1 {
		existingLineItem := existingLineItems[0]
		existingLineItem.Quantity += e.Quantity
		return m.update(existingLineItem)
	}
	return nil
}
