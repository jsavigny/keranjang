package line_item

import "keranjang/entity"

type Service interface {
	GetByOrderID(order_id int64) ([]*entity.LineItem, error)
}
