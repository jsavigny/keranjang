package product

import "keranjang/entity"

type Repository interface {
	GetBySKU(sku string) (*entity.Product, error)
	Update(product *entity.Product) error
}
