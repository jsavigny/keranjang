package repository

import (
	"database/sql"
	"fmt"
	"keranjang/entity"
	"keranjang/product"
)

type mysqlRepository struct {
	Conn *sql.DB
}

func NewMysqlRepository(Conn *sql.DB) product.Repository {
	return &mysqlRepository{Conn}
}

func (m *mysqlRepository) fetch(query string, args ...interface{}) ([]*entity.Product, error) {
	rows, err := m.Conn.Query(query, args...)
	if err != nil {
		return nil, err
	}

	defer func() {
		rows.Close()
	}()

	result := make([]*entity.Product, 0)
	for rows.Next() {
		t := new(entity.Product)
		err = rows.Scan(
			&t.ID,
			&t.SKU,
			&t.Name,
			&t.Price,
			&t.Stock,
		)

		if err != nil {
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlRepository) GetBySKU(sku string) (res *entity.Product, err error) {
	query := `SELECT id,sku,name, price, stock
  						FROM products WHERE sku = ?`

	list, err := m.fetch(query, sku)
	if err != nil {
		return nil, err
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return nil, fmt.Errorf("Product Not Found")
	}

	return res, nil
}

func (m *mysqlRepository) Update(p *entity.Product) error {
	query := `UPDATE products set sku=?, name=?, price=?, stock=? WHERE id = ?`

	stmt, err := m.Conn.Prepare(query)
	if err != nil {
		return nil
	}

	res, err := stmt.Exec(p.SKU, p.Name, p.Price, p.Stock, p.ID)
	if err != nil {
		return err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", affect)

		return err
	}

	return nil
}
